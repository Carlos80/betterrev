package models;

import static javax.persistence.GenerationType.IDENTITY;

import org.joda.time.DateTime;

import play.db.ebean.Model;
import play.utils.dao.BasicModel;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * PullReviewEvent that represents a lifecycle event associated with a specific
 * PullReview.
 */
@Entity
public class PullReviewEvent extends Model implements BasicModel<Long> {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, PullReviewEvent> find = new Model.Finder<>(Long.class, PullReviewEvent.class);

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @ManyToOne
    public PullReview pullReview;

    @Enumerated(EnumType.STRING)
    public PullReviewEventType pullReviewEventType;

    public String linkToExternalInfo;

    public DateTime createdDate;

    private PullReviewEvent() {
        this.createdDate = new DateTime();
    }

    public PullReviewEvent(PullReviewEventType pullReviewEventType) {
        this();
        this.pullReviewEventType = pullReviewEventType;
    }

    public PullReviewEvent(PullReviewEventType pullReviewEventType, String linkToExternalInfo) {
        this(pullReviewEventType);
        this.linkToExternalInfo = linkToExternalInfo;
    }

    @Override
    public String toString() {
        return "PullReviewEvent [id=" + id + ", pullReview=" + pullReview + ", pullReviewEventType="
                + pullReviewEventType + ", linkToExternalInfo=" + linkToExternalInfo + ", createdDate=" + createdDate
                + "]";
    }

    @Override
    public Long getKey() {
        return id;
    }

    @Override
    public void setKey(Long key) {
        id = key;
    }
}
